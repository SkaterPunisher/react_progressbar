import React from 'react'

const Discription = ({ items }) => {
  return (
    <ul className='flex justify-between'>
        {items.map((item) => {
            return (
                <li className='flex'>
                    <div className='mr-2 last:mr-0 w-[20px] h-[20px] rounded-full' style={{backgroundColor: `${item.color}`}}></div>
                    <p>{item.name} : {item.value} ({item.percent}%)</p>
                </li>
            )
        })}
    </ul>
  )
}

export default Discription