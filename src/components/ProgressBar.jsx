import React from 'react';
import { v4 as uuidv4 } from 'uuid';
import ProgressBarVertical from './ProgressBarVertical';

const ProgressBar = ({ items }) => {
  return (
    <div className='flex w-[95vh] h-[30px] mb-10'>
      {items.map((item) => {
        return (
          <ProgressBarVertical items={item} key={uuidv4()}/>
        )
      })}
    </div>
  );
};

export default ProgressBar;