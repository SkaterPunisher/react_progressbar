import React from 'react';
import { v4 as uuidv4 } from 'uuid';

const ProgressBarVertical = ({ items }) => {
  let arr = items.arr;

  return (
    <>
      {arr.map(() => {
        return (
          <div
            className={`h-full rounded-lg mr-[3px] last:mr-0`}
            key={uuidv4()}
            style={{ width: `1%`, backgroundColor: `${items.color}`}}
          ></div>
        );
      })}
    </>
  );
};

export default ProgressBarVertical;
