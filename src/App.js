import './App.css';
import Discription from './components/Discription';
import ProgressBar from './components/ProgressBar';

function App() {
  const items = [
    { name: 'Sold', color: '#BD1FBE', value: 1000 },
    { name: 'Got free', color: '#FC64FF', value: 360 },
    { name: 'Burned', color: '#88f56d', value: 240 },
    { name: 'Free float', color: '#b8b8b8', value: 10 },
  ];
  let sum = 0;
  items.forEach((item) => {
    sum += item.value;
  });
  items.forEach((item) => {
    item.percent = Math.ceil((item.value * 100) / sum);
    item.arr = Array(item.percent + 1).join('*').split('')
  });
  console.log(items)

  const f = (a) => a ||2
  let x =f(f(f(0))) + f(null) + f(undefined) + f('')
  console.log(x)

  return (
    <>
      <ProgressBar items={items} />
      <Discription items={items} />
    </>

  );
}

export default App;
